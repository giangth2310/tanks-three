import React from 'react'
import { promises as fs } from 'fs'
import path from 'path'
import { Col, Layout, Row, Grid, Image as AntImage } from 'antd'
import styles from '../styles/Home.module.css'
import Image from 'next/image'
import Head from 'next/head'
import { Page, CustomChat } from 'react-facebook'

const { Header, Content, Footer } = Layout
const { useBreakpoint } = Grid

const Home = ({ photos }) => {
  const screens = useBreakpoint()
  let subtitle = (
    <h3>
      <div>Facebook Page: <a href="https://facebook.com/tanksvn" target="_blank" rel="noopener">https://fb.com/tanksvn</a></div>
      <div>Hotline/Zalo: <a href="tel:+84857379966">085.737.9966</a></div>
      <div>Shopee: <a href="https://shopee.vn/longtv0910" target="_blank" rel="noopener">https://shopee.vn/longtv0910</a></div>
    </h3>
  )

  if (screens["lg"] || screens["xl"] || screens["xxl"]) {
    subtitle = (
      <h3>
        <div>Facebook Page: <a href="https://facebook.com/tanksvn" target="_blank" rel="noopener">https://facebook.com/tanksvn</a></div>
        <div>Hotline/Zalo: <a href="tel:+84857379966">085.737.9966</a></div>
        <div>Shopee: <a href="https://shopee.vn/longtv0910" target="_blank" rel="noopener">https://shopee.vn/longtv0910</a></div>
      </h3>
    )
  }

  return (
    <Layout className={styles.container}>
      <Head>
        <link
          rel="preload"
          href="/fonts/armalite_rifle.ttf"
          as="font"
          crossOrigin=""
        />
      </Head>

      <Header className={styles.header}>
        <Image src="/images/logo.png" width={250} height={112} alt="logo" />
        <h1>Tanks.vn</h1>
        {subtitle}
      </Header>

      <Content className={styles.content}>
        <Row justify="center" align="middle" gutter={[16, { xs: 8, sm: 16, md: 24, lg: 32 }]} >
          <AntImage.PreviewGroup>
            {photos.map(({ src, id, name, alt_text, link }) => {
              return (
                <Col key={id} xs={12} sm={8} md={6} >
                  <div className={styles.product}>
                    <AntImage src={src} width="100%" alt={name || alt_text} />
                    <a href={link} target="_blank" rel="noopener">{name || alt_text}</a>
                  </div>
                </Col>
              )
            })}
          </AntImage.PreviewGroup>
        </Row>
      </Content>

      <div className={styles.fbPage}>
        <Page href="https://www.facebook.com/tanksvn" />
        <CustomChat pageId={process.env.NEXT_PUBLIC_FB_PAGE_ID} />
      </div>

      <Footer className={styles.footer}>
        Rolled out by Tanks.vn
      </Footer>
    </Layout>
  )
}

export async function getStaticProps() {
  let photos = []

  try {
    const filePath = path.join(process.cwd(), process.env.FB_PHOTOS_FILE)
    const photosJSON = await fs.readFile(filePath, 'utf8')
    photos = JSON.parse(photosJSON).reverse()
  } catch (error) {
    console.log(error)
  }

  return {
    props: {
      photos
    },
    revalidate: 1,
  }
}

export default Home