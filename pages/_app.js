import '../styles/globals.css'
import { FacebookProvider, useFacebook } from 'react-facebook'
import 'antd/dist/antd.css'
import Head from 'next/head'
import React from 'react'

export const FBAPIContext = React.createContext()

const withFBAPI = (WrappedComponent) => {
  return (props) => (
    <FacebookProvider appId={process.env.NEXT_PUBLIC_FB_APP_ID} chatSupport={false}>
      <WrappedComponent {...props} />
    </FacebookProvider>

  )
}

function MyApp({ Component, pageProps }) {
  return (
    <FBAPIContext.Provider value={{ }}>
      <Head>
        <title>Tanks.vn - Cửa hàng mô hình</title>
        <meta name="description" content="Cung cấp các loại kit mô hình quân sự các chủng loại 
          Địa chỉ: Hoàng Mai, Hà Nội
          Ship cod toàn quốc
          Sđt: 0857379966 - Vĩnh Long (1988)">
        </meta>
        <meta property="og:image" content="/images/ogimage.jpg" key="ogimage" />
      </Head>
      <Component {...pageProps} />
    </FBAPIContext.Provider>
  )
}

export default withFBAPI(MyApp)
