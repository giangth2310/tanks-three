import React, { useState, useEffect } from 'react'
import { Layout, Menu, Row, Col, Divider} from 'antd'
import {
  DashboardOutlined
} from '@ant-design/icons'
import Image from 'next/image'
import styles from '../../styles/Exchange.module.css'
import io from 'socket.io-client'
const _ = require('lodash')
let socket
const { Header, Content, Sider } = Layout
const BidRow = (bid, index) => {
  return (
  <Row className={styles.border_bottom}>
    <Col span={12}>
      {bid.quantity}
    </Col>
    <Col span={12} className={styles.border_right + (index === 0 ? " warning" : "")}>
      {bid.rate}
    </Col>
  </Row>
  )
}

const AskRow = (ask, index) => {
  return (
  <Row className={styles.border_bottom}>
    <Col span={12} className={(index === 0 ? " warning" : "")}>
      {ask.rate}
    </Col>
    <Col span={12}>
      {ask.quantity}
    </Col>
  </Row>
  )
}

const Exchange = ({ loading }) => {
  const [bids, setBids] = useState([])
  const [asks, setAsks] = useState([])

  useEffect(() => socketInitializer(), [])

  const socketInitializer = async () => {
    await fetch('/api/socket');
    socket = io()

    socket.on('connect', () => {
      console.log('connected')
    })

    socket.on('update-orderbook', data => {
      const orderbook = data.orderbook
      setBids(_.orderBy(orderbook.bidDeltas, ['rate'], ['desc']))
      setAsks(_.orderBy(orderbook.askDeltas, ['rate'], ['asc']))
    })
  }

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider theme="light" className={styles.sider}>
        <div className={styles.logo}>
          <a href="/" rel="noopener">
            <Image src="/images/logo.png" width={142} height={64} />
          </a>
        </div>
        <Menu theme="light" defaultSelectedKeys={['1']} mode="inline">
          <Menu.Item key="1" icon={<DashboardOutlined />}>
            Dashboard
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout>
        <Header className={styles.header}>
          Exchange Orderbook
        </Header>
        <Content className={styles.content}>
        <div>
          <Row className={styles.exchange_header}>
            <Col span={12}>
              <Row>
                <Col span={12}>Size</Col>
                <Col span={12}>Bid</Col>
              </Row>
              
            </Col>
            <Col span={12}>
              <Row>
                <Col span={12}>Ask</Col>
                <Col span={12}>Size</Col>
              </Row>
            </Col>
          </Row>
          <Row className={styles.exchange_content}>
            <Col span={12}>
              {bids.map((bid, index) => {return BidRow(bid, index)})}
            </Col>
            <Col span={12}>
              {asks.map((ask, index) => {return AskRow(ask, index)})}
            </Col>
          </Row>
          <Divider orientation="left"></Divider>
          <Row>
            <Col span={12}>
              Total amount (size * bid): &nbsp;
              <span className={styles.sum_info}>
                {_.sumBy(bids, (bid) => {
                  return bid.rate * bid.quantity
                }).toFixed(6)}
              </span>
            </Col>
            <Col span={12}>
              Total size of asks: &nbsp; 
              <span className={styles.sum_info}>
                {_.sumBy(asks, (ask) => {
                  return parseFloat(ask.quantity)
                }).toFixed(6)}
              </span>
            </Col>
          </Row>
        </div>
        </Content>
      </Layout>
    </Layout>
  )
}

export default Exchange