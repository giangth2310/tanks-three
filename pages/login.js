import React, { useContext, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import styles from '../styles/Login.module.css'
import { FBAPIContext } from './_app'
import { Button } from 'antd'
import { useFacebook } from 'react-facebook'

const Login = () => {
  const { isLoading: isLoadingFB, init, error: errorFB } = useFacebook();
  const router = useRouter()
  const [redirecting, setRedirecting] = useState(false)
  //const { api } = useContext(FBAPIContext)

  useEffect(async () => {
    // const api = await init();
    // const { status } = await api.getLoginStatus()
    // console.log(status)
    // if (status === 'connected') {
    //   setRedirecting(true)
    //   router.push('/dashboard')
    // }
    // console.log(isReady);
    // if (isReady) {
    //   const { status } = await api.getLoginStatus()
    //   console.log(status)
    //   if (status === 'connected') {
    //     setRedirecting(true)
    //     router.push('/dashboard')
    //   }
    // }
  }, [])

  const onLogin = async () => {
    try {
      const api = await init();
      await api.login({ scope: "pages_read_engagement,email" })
      const profile = await api.api("me?fields=id,name,email")
      if (!['giangth2310@gmail.com', 'longtv0910@gmail.com'].includes(profile?.email)) {
        console.log("logout")
        await api.logout()
      }
      setRedirecting(true)
      router.push('/dashboard')
    } catch (error) {
      console.log(error)
    }
  }

  if (redirecting) {
    return "Redirecting"
  }

  return (
    <div className={styles.login}>
      <Button type='primary' onClick={onLogin}>Login via Facebook</Button>
    </div>
  )
}

export default Login