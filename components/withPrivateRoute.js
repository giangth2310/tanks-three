import React, { useEffect, useContext, useState } from 'react'
import { useRouter } from 'next/router'
import { FBAPIContext } from '../pages/_app'
import { Spin } from 'antd'
import { useFacebook } from 'react-facebook'

const withPrivateRoute = (WrappedComponent) => {
  return (props) => {
    const router = useRouter()
    const [loading, setLoading] = useState(true)
    const { isLoading: isLoadingFB, init, error: errorFB } = useFacebook();

    useEffect(async () => {
      const api = await init();
      const { status } = await api.getLoginStatus()
      if (status === 'connected') {
        setLoading(false)
      } else {
        router.push('/login')
      }
      // if (isReady) {
      //   const { status } = await api.getLoginStatus()
      //   if (status === 'connected') {
      //     setLoading(false)
      //   } else {
      //     router.push('/login')
      //   }
      // }
    }, [])

    if (loading) {
      return (
        <Spin tip='Logging in...'>
          <WrappedComponent {...props} loading={loading} />
        </Spin>
      )
    }

    return <WrappedComponent {...props} loading={loading} />
  }
}

export default withPrivateRoute